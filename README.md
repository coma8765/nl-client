# Client for test task for NL
> made by [@coma8765](https://gitlab.com/coma8765)

**Overview**
- [Run in Docker guide](#in-docker)
- [Bare metal run](#bare)

## In Docker

### Build
**Copy and fill .env.example to .env**
```shell
docker build \
  -e BASE_API_URL=BASE_API_URL \
  -f templates/Dockerfile \
  -t nl-client \
  .
```

### Run
```shell
docker run -p 3000:3000 nl-client
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Run
```shell
docker build \
  -f templates/Dockerfile \
  -t nl-client \
  .
```

## Bare

### Install dependencies
```shell
yarn install
```

### Run
**Copy and fill .env.example to .env**
```bash
yarn dev
```

### Build
**Copy and fill .env.example to .env**
```bash
yarn build
```

### Production run
**Copy and fill .env.example to .env**
```bash
yarn start
```

### Export static
**Copy and fill .env.example to .env**
```bash
yarn build
yarn export
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
