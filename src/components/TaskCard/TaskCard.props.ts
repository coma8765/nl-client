import { DetailedHTMLProps, HTMLAttributes } from 'react';
import {Task} from '@/shared/types/task';

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	task: Task;
	onDelete?: () => void;
}

export default Props;
