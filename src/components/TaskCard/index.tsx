import React, {useState} from 'react';
import Props from '@/components/TaskCard/TaskCard.props';
import {Button, Card} from 'antd';
import {useMutation} from '@tanstack/react-query';
import {updateTask} from '@/shared/api/task';

const TaskCard: React.FC<Props> = (({className, onDelete, task}) => {
	const [removed, setRemoved] = useState<boolean>(false);

	const {mutate} = useMutation(() => updateTask({taskId: task.id, ref: {status: "done"}}), {
		onSuccess: () => setRemoved(true),
	});

	return (
		<Card title={task.title} extra={
			<div className={"flex flex-row"}>
				<Button type="default" className="mx-0.5" href={`/tasks/${task.id}/edit`}>Edit</Button>
				{task.status !== "done" && <Button type="default" className="mx-0.5" onClick={() => mutate()}>Delete</Button>}
			</div>
		} key={task.id} className={"m-2 w-[400px] overflow-hidden " + className + (removed ? " hidden": "")}>
			<p>{task.status}</p>
			{task.description && <p>{task.description}</p>}
			<p>Deadline: {new Date(task.deadline).toDateString()}</p>
			<p>Created at: {new Date(task.created_at).toDateString()}</p>
		</Card>
	);
});

export default TaskCard;
