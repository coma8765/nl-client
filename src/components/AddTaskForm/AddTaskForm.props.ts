import { DetailedHTMLProps, HTMLAttributes } from 'react';

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	onSubmit: (values: any) => void;
	task?: Record<string, any>;
}

export default Props;
