import React from 'react';
import Props from '@/components/AddTaskForm/AddTaskForm.props';
import {Button, DatePicker, Form, Input} from 'antd';
import dayjs from 'dayjs';

const AddTaskForm: React.FC<Props> = ({className, onSubmit, task}) => {
	return (
		<Form
			name="add_task"
			className={`add-task-form ${className}`}
			initialValues={task && {title: task.title, description: task.description, deadline: dayjs(new Date(task.deadline))}}
			onFinish={onSubmit}
		>
			<Form.Item
				name="title"
				rules={[{required: true, message: `Please input title`}]}
			>
				<Input placeholder="Title"/>
			</Form.Item>
			<Form.Item
				name="description"
				rules={[{required: true, message: `Please input description`}]}
			>
				<Input placeholder="description"/>
			</Form.Item>
			<Form.Item
				name="deadline"
				rules={[{required: true, message: `Please input deadline`}]}
			>
				<DatePicker size={"middle"}/>
			</Form.Item>
			<Form.Item>
				<Button type="primary" htmlType="submit" className="bg-blue-500 login-form-button mr-2">
					{task ? "Update task" : "Create task"}
				</Button>
			</Form.Item>
		</Form>
	);
};

export default AddTaskForm;
