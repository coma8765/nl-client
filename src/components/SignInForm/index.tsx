import React from 'react';
import Props from '@/components/SignInForm/SignIn.props';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';

const SignInForm: React.FC<Props> = ({className, onSubmit, warning = null}) => {
	return (
		<Form
			name="normal_login"
			className={`login-form ${className}`}
			initialValues={{ remember: true }}
			onFinish={onSubmit}
		>
			<Form.Item
				name="login"
				rules={[{ required: true, message: 'Please input your Login!' }]}
			>
				<Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Login" />
			</Form.Item>
			<Form.Item
				name="password"
				rules={[{ required: true, message: 'Please input your Password!' }]}
			>
				<Input
					prefix={<LockOutlined className="site-form-item-icon" />}
					type="password"
					placeholder="Password"
				/>
			</Form.Item>
			<Form.Item>
				<a className="login-form-forgot" href="">
					Forgot password (not implement)
				</a>
			</Form.Item>
			{warning && <div className={"my-2 text-red-600"}>{warning}</div>}
			<Form.Item>
				<Button type="primary" htmlType="submit" className="login-form-button mr-2 bg-blue-500">
					Log in
				</Button>
				Or <a href="/signup">register now!</a>
			</Form.Item>
		</Form>
	)
};

export default SignInForm;
