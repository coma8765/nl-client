import React from 'react';
import Props from '@/components/UserCard/UserCard.props';
import {Card, Col, Row} from 'antd';
import {getMe} from '@/shared/api/user';
import {useQuery} from '@tanstack/react-query';

const UserCard: React.FC<Props> = ({className}) => {
	const {data} = useQuery(["current-user"], getMe)

	return (
		<Card className={className} title="Profile" bordered={false}>
			{data && (
				<>
					<div>Id: {data.id}</div>
					<div>Login: {data.login}</div>
					<div>Name: {data.name}</div>
					<div>Surname: {data.surname}</div>
					<div>Created at: {data.created_at}</div>
				</>
			)}
		</Card>
	)
};

export default UserCard;
