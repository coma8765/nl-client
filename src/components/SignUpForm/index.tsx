import React from 'react';
import Props from '@/components/SignUpForm/SignUp.props';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input } from 'antd';

const LoginForm: React.FC<Props> = ({className, onSubmit, warning}) => {
	return (
		<Form
			name="normal_login"
			className={`login-form ${className}`}
			onFinish={onSubmit}
		>
			<Form.Item
				name="login"
				rules={[{ required: true, message: 'Please input your Login!' }]}
			>
				<Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Login" />
			</Form.Item>

			<Form.Item
				name="name"
				rules={[{ required: true, message: 'Please input your name!' }]}
			>
				<Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Name" />
			</Form.Item>

			<Form.Item
				name="surname"
				rules={[{ required: true, message: 'Please input your Surname!' }]}
			>
				<Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Surname" />
			</Form.Item>

			<Form.Item
				name="password"
				rules={[{ required: true, message: 'Please input your Password!' }]}
			>
				<Input
					prefix={<LockOutlined className="site-form-item-icon" />}
					type="password"
					placeholder="Password"
				/>
			</Form.Item>

			<Form.Item>
				<a className="login-form-forgot" href="">
					Forgot password (not implement)
				</a>
			</Form.Item>
			{warning && <div className={"my-2 text-red-600"}>{warning}</div>}
			<Form.Item>
				<Button type="primary" htmlType="submit" className="login-form-button bg-blue-500 mr-2">
					Log in
				</Button>
				Or <a href="/signin">login now!</a>
			</Form.Item>
		</Form>
	)
};

export default LoginForm;
