import { DetailedHTMLProps, HTMLAttributes } from 'react';
import {Task} from '@/shared/types/task';

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	onSubmit: (data: Record<string, any>) => void;
	warning?: string;
}

export default Props;
