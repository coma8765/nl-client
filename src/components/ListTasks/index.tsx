import React from 'react';
import Props from '@/components/ListTasks/ListTask.props';
import TaskCard from '@/components/TaskCard';

const ListTasks: React.FC<Props> = ({className, tasks}) => {
	return (
		<div className={"flex flex-row flex-wrap"}>
			{tasks.map(task => <TaskCard
				key={task.id}
				task={task}
			/>)}
		</div>
	);
};

export default ListTasks;
