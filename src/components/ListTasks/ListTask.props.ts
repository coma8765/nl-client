import { DetailedHTMLProps, HTMLAttributes } from 'react';
import {Task} from '@/shared/types/task';

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	tasks: Task[];
}

export default Props;
