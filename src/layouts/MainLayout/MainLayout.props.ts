import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import {AntdIconProps} from '@ant-design/icons/es/components/AntdIcon';

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	activePath?: string;
	tabs?: {
		path: string;
		icon: React.ForwardRefExoticComponent<AntdIconProps & React.RefAttributes<HTMLSpanElement>>;
		label: string
	}[]
}

export default Props;
