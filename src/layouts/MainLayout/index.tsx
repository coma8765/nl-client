import Props from '@/layouts/MainLayout/MainLayout.props';
import React, {useEffect} from 'react';
import {
	CheckCircleOutlined,
	ClockCircleOutlined,
	PlusCircleOutlined,
	UploadOutlined,
	UserOutlined,
} from '@ant-design/icons';
import {Button, Layout, Menu, theme} from 'antd';
import {Router, useRouter} from 'next/router';
import {AntdIconProps} from '@ant-design/icons/es/components/AntdIcon';
import {log} from 'util';

const { Header, Content, Footer, Sider } = Layout;
const Tabs: ({
	path: string;
	icon: React.ForwardRefExoticComponent<AntdIconProps & React.RefAttributes<HTMLSpanElement>>;
	label: string
})[] = [
	{icon: UserOutlined, label: "Аккаунт", path: "/me"},
	{icon: PlusCircleOutlined, label: "Дабавить", path: "/tasks/add"},
	{icon: ClockCircleOutlined, label: "Выполняются", path: "/tasks/process"},
	{icon: CheckCircleOutlined, label: "Выполнены", path: "/tasks/done"},
]

const MainLayout: React.FC<Props> = ({tabs = Tabs, activePath = "/me", children}) => {
	const {
		token: { colorBgContainer },
	} = theme.useToken();
	const router = useRouter();

	useEffect(
		() => {
			if (localStorage.getItem("token") === null) router.push("/signin")
		}
	);

	return (<Layout className={"min-w-max"} style={{minHeight: "100vh"}}>
			<Sider
				className={"w-screen"}
				breakpoint="lg"
				collapsedWidth="0"
				onBreakpoint={(broken) => {}}
				onCollapse={(collapsed, type) => {}}
			>
				<h1 className="text-lg text-white text-center pt-4">ToDo IT</h1>
				<Menu
					theme="dark"
					mode="inline"
					onClick={(r) => router.push(r.key)}
					defaultSelectedKeys={[activePath]}
					items={Object.values(tabs).map(
						({icon, label, path}, index) => ({
							key: path,
							icon: React.createElement(icon),
							label,
							path,
						}),
					)}
				/>
				<Button type="default" className="text-white mx-12 my-4" onClick={() => {
					sessionStorage.removeItem("token");
					router.push("/signin").then();
				}}>Выйти</Button>
			</Sider>
			<Layout>
				<Header style={{ padding: 0, background: colorBgContainer }} />
				<Content style={{ margin: '24px 16px 0' }}>
					{children}
				</Content>
				<Footer style={{ textAlign: 'center' }}>Coma Dev ©2023 Created by coma8765</Footer>
			</Layout>
		</Layout>
	);
}

export default MainLayout;
