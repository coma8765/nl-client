import {Token, User, UserAuth, UserRef} from '@/shared/types/user';
import instance from '@/shared/api/axios';

export const signup = (ref: UserRef): Promise<User> => {
	return instance.post("/v1/users/signup", ref)
		.then(r => r.data);
}

export const signin = (ref: UserAuth): Promise<Token> => {
	return instance.post("/v1/users/signin", ref)
		.then(r => r.data);
};


export const getMe = (): Promise<User> => {
	return instance.get("/v1/users/user").then(r => r.data)
}
