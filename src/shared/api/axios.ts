import axios from 'axios';

const API_URI = process.env.BASE_API_URL || "http://127.0.0.1:8000"

const instance = axios.create({
	baseURL: API_URI,
	timeout: 1000,
	headers: {
		"Content-Type": "application/json",
	},
});

instance.interceptors.request.use(function (config) {
	config.headers.Authorization = localStorage.getItem("token");
	return config;
}, function (error) {
	sessionStorage.removeItem("token");
	window.location.replace("/signin");
	return Promise.reject(error);
});

export default instance;
