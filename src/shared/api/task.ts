import instance from '@/shared/api/axios';
import {Task, TaskRef, TaskUpdate} from '@/shared/types/task';
import {List} from '@/shared/types/dto';

export const createTask = (ref: TaskRef): Promise<Task> => {
	return instance.post("/v1/tasks", ref)
		.then(r => r.data);
}

export const getTasks = ({page = 1, statuses = []}: { page: number, statuses?: string[]}): Promise<List<Task[]>> => {
	let path = `/v1/tasks/user?page=${page}`;
	statuses.forEach(status => path += `&status=${status}`);

	return instance.get(path)
		.then(r => r.data);
}

export const getTask = ({id}: {id: string}): Promise<Task> => {
	return instance.get(`/v1/tasks/${id}`)
		.then(r => r.data);
}

export const updateTask = ({taskId, ref}: {taskId: string, ref: TaskUpdate}): Promise<void> => {
	return instance.patch(`/v1/tasks/${taskId}`, ref)
		.then(r => r.data);
}
