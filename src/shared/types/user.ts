export interface User {
	id: string;
	login: string;
	name: string;
	surname: string;
	created_at: string;
}

export interface UserRef {
	login: string;
	name: string;
	surname: string;
	password: string;
}

export interface UserAuth {
	login: string;
	password: string;
}

export interface Token {
	type: string;
	token: string;
}
