export const TaskStatuses = ["created", "in_process", "done"]

export interface TaskRef {
	title: string;
	description: string;
	status: string;
	deadline: string;
	created_at: string;
}

export interface Task extends TaskRef {
	id: string;
}

export interface TaskUpdate {
	title?: string;
	description?: string;
	status?: string;
	deadline?: string;
	created_at?: string;
}
