export interface List<T> {
	list: T;
	has_next: boolean;
	page: number;
}
