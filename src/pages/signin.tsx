import React, {useCallback, useState} from 'react';
import SignInForm from '../components/SignInForm';
import {useRouter} from 'next/router';
import {UserRef} from '@/shared/types/user';
import {signin} from '@/shared/api/user';
import {AxiosError} from 'axios';


export default function Home() {
	const router = useRouter();
	const [warning, setWarning] = useState<string | undefined>();

	const signupCallback = useCallback(
		async (data: UserRef) => {
			try {
				const token = await signin(data);

				localStorage.setItem("token", token.type + " " + token.token);
				await router.push("/");
			} catch (e: unknown) {
				if (e instanceof AxiosError && e.response){
					setWarning(e.response.data.detail);
				}
			}
		},
		[router],
	);

	return (
		<div className={"w-screen h-screen"}>
			<div className="w-[400px] mx-auto py-52">
				<h1 className={"text-lg font-bold p-2 text-center"}>SignIn</h1>
				<SignInForm onSubmit={signupCallback} warning={warning}/>
			</div>
		</div>
	);
};
