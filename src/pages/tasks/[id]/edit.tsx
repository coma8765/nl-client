import MainLayout from '@/layouts/MainLayout';
import React from 'react';
import AddTaskForm from '@/components/AddTaskForm';
import {useMutation, useQuery} from '@tanstack/react-query';
import {getTask, updateTask} from '@/shared/api/task';
import {useRouter} from 'next/router';
import {TaskRef} from '@/shared/types/task';

export default function Tasks() {
	const router = useRouter();
	const id: string = router.query.id as string;

	const {data} = useQuery(
		["task", id],
		() => getTask({id}),
		{enabled: !!id},
	);

	const {mutate} = useMutation(
		(data: TaskRef) => updateTask({taskId: id, ref: data}), {
			onSuccess: () => router.push("/tasks/process"),
		}
	)

	return (
		<MainLayout activePath={"/tasks"}>
			<h1 className="text-xl font-bold pb-2">Update</h1>
			{(data !== undefined) ? <AddTaskForm task={data} onSubmit={mutate}/> : <></>}
		</MainLayout>
	);
};
