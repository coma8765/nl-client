import MainLayout from '@/layouts/MainLayout';
import React from 'react';
import UserCard from '@/components/UserCard';

export default function Tasks() {
	return (
		<MainLayout activePath={"/tasks"}>
			<UserCard />
		</MainLayout>
	);
};
