import MainLayout from '@/layouts/MainLayout';
import React, {useState} from 'react';
import ListTasks from '@/components/ListTasks';
import {useQuery} from '@tanstack/react-query';
import {getTasks} from '@/shared/api/task';
import {Button} from 'antd';
import {Task} from '@/shared/types/task';

export default function DoneTasks() {
	const [page, setPage] = useState(1);
	const [hasNext, setHasNext] = useState(true);
	const [allData, setAllData] = useState<Task[]>([]);


	useQuery(
		["tasks", page],
		async (): Promise<Task[]> => {
			const res = await getTasks({page, statuses: ["done"]});
			setHasNext(res.has_next);
			return res.list;
		},
		{
			onSuccess: (newData) => setAllData(allData?.concat(newData.filter(task => !allData.includes(task)))),
		},
	);

	return (
		<MainLayout activePath={"/tasks/done"}>
			<h1 className="text-lg font-bold">Done tasks</h1>
			{allData && <ListTasks tasks={allData}/>}
			<div className={"flex flex-row justify-center mt-2"}>
				{hasNext && <Button type="default" onClick={() => setPage(page + 1)}>Load more</Button>}
			</div>
		</MainLayout>
	);
};
