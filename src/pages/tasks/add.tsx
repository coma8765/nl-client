import MainLayout from '@/layouts/MainLayout';
import React, {useCallback} from 'react';
import AddTaskForm from '@/components/AddTaskForm';
import {useMutation} from '@tanstack/react-query';
import {createTask} from '@/shared/api/task';
import {useRouter} from 'next/router';

export default function TasksAdd() {
	const router = useRouter()
	const {mutate} = useMutation(createTask, {
		onSuccess: () => router.push("/tasks/process"),
	})

	return (
		<MainLayout activePath={"/tasks/add"}>
			<h1 className="text-lg font-bold">Add task</h1>
			<AddTaskForm onSubmit={mutate} className={"w-[400px]"}/>
		</MainLayout>
	);
};
