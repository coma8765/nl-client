import MainLayout from '@/layouts/MainLayout';
import React from 'react';
import UserCard from '@/components/UserCard';

export default function Home() {
  return (
    <MainLayout activePath={"/me"}>
      <UserCard />
    </MainLayout>
  );
};
