import React, {useCallback, useState} from 'react';
import SignUpForm from '@/components/SignUpForm';
import {signin, signup} from '@/shared/api/user';
import {UserRef} from '@/shared/types/user';
import {useRouter} from 'next/router';
import {AxiosError} from 'axios';


export default function SignUp() {
	const router = useRouter();
	const [warning, setWarning] = useState<string | undefined>()
	const signupCallback = useCallback(
		async (data: UserRef) => {
			try {
				const user = await signup(data);
				const token = await signin(data);

				localStorage.setItem("token", token.type + " " + token.token);
				await router.push("/");
			} catch (e: unknown) {
				if (e instanceof AxiosError && e.response){
					setWarning(e.response.data.detail);
				}
			}
		},
		[router],
	);

	return (
		<div className={"w-screen h-screen"}>
			<div className="w-[400px] mx-auto py-52">
				<h1 className={"text-lg font-bold p-2 text-center"}>SignUp</h1>
				<SignUpForm warning={warning} onSubmit={data => signupCallback(data as UserRef)}/>
			</div>
		</div>
	);
};
